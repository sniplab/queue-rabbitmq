# receive.py
# вторая программа receive.py будет получать сообщения из очереди и
# выводить их на экран.

# 1. подключиться к RabbitMQ
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
channel.queue_declare(queue='hello')


# 2. подписка на канал с использованием callback функции
# При получении каждого сообщения библиотека Pika вызывает эту callback функцию
def callback(ch, method, properties, body):
    print(" [x] Received %r" % (body,))


# 3. callback функция будет получать сообщения из очереди с именем «hello»
channel.basic_consume(
    on_message_callback=callback,
    queue='hello',
    # no_ack=True  # old
    auto_ack=False,
)


# 4. запуск бесконечного процесса,
# который ожидает сообщения из очереди и вызывает callback функцию
print('[*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
