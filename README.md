# RabbitMQ

[Theory](/theory.md)

## Installation

    brew install rabbitmq
    brew services start rabbitmq
    brew services restart rabbitmq

    rabbitmq-plugins enable rabbitmq_management

## Installation [docker]

`docker run --rm -it --hostname my-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management`

## CLI

!!! only with SUDO !!!!!

`sudo rabbitmqctl list_queues`

```
Timeout: 60.0 seconds ...
Listing queues for vhost / ...
name	messages
hello	1
```

**********************************************************************

## Monitoring

### [1] Admin section 

- default username and password, guest:guest

### [2] with command in terminal

- brew install watch

[в нашей компании используют более удобный скрипт мониторинга:]
[
скрипт выводит и обновляет каждые 2 секунды таблицу со списком очередей:
имя очереди;
количество сообщений в обработке;
количество сообщений готовых к обработке;
общее количество сообщений;
устойчивость очереди к перезагрузке сервиса;
является ли временной очередью; количество подписчиков
]


#### old~
watch 'sudo /usr/sbin/rabbitmqctl list_queues name messages_unacknowledged messages_ready messages durable auto_delete consumers | grep -v "\.\.\." | sort | column -t;'

#### working version ("sudo rabbitmqctl list_queues")
watch 'sudo rabbitmqctl list_queues name messages_unacknowledged messages_ready messages durable auto_delete consumers | grep -v "\.\.\." | sort | column -t;'

**********************************************************************

#### References: 

- https://habr.com/ru/post/149694/
- https://codeburst.io/get-started-with-rabbitmq-on-docker-4428d7f6e46b

[Official Tutorials]: https://www.rabbitmq.com/tutorials/tutorial-one-python.html

