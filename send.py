#!/usr/bin/env python
import pika

# send.py будет просто отправлять одно сообщение в очередь.

# 1. подключились к брокеру сообщений, находящемуся на локальном хосте
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
# Для подключения к брокеру, находящемуся на другой машине, достаточно
# заменить «localhost» на IP адрес этой машины.

# 2. назовем queue «hello»
channel.queue_declare(queue='hello')

# в RabbitMQ сообщения не отправляются непосредственно в очередь,
# они должны пройти через exchange (точка обмена)

# 3. точку обмена по-умолчанию можно определить, указав пустую строку
# routing_key: name of the queue
channel.basic_publish(exchange='', routing_key='hello', body=b'Hello World!')
print("[x] Sent 'Hello World!'")

# 4. Перед выходом из программы необходимо убедиться, что буфер был очищен
# и сообщение дошло до RabbitMQ. В этом можно быть уверенным,
# если использовать безопасное закрытие соединения с брокером.
connection.close()
